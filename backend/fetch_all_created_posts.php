<?php
/**
 * Vai buscar todos os posts existentes na base de dados
 */
$path='./img/';
$sql = "SELECT post.mensagem,tema.categoria,post.time,users.firstname,users.lastname,post.photo
        FROM db_posts.tema, db_posts.post, db_users.users WHERE post.id_post IS NOT NULL ORDER BY time DESC ";
try {
    $result = $PDO_connect_posts->query($sql);
    $result->setFetchMode(PDO::FETCH_ASSOC);
    if ($result > 0) {
        while ($row = $result->fetch()) {
            $categoria = $row['categoria'];
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];
            $mensagem_post = $row['mensagem'];
            $timestamp = $row['time'];
            $photo = $row['photo'];
            echo '<div style="border: 3px solid gold; padding-left: 2%; margin-top: 1%; margin-left: 2%; margin-right: 2%;margin-bottom: 1%" >';
            echo '<h3>Tema: ' . $categoria . '</h3>';
            if($photo != null) {
                echo '<img src="' . $path. $photo . '" style="max-width=150px; max-height: 150px">';
            $photo=null;
            }
            echo'<p>Mensagem: ' . $mensagem_post . '</p><p style="bold">Utilizador: ' . $firstname . ' ' . $lastname . '</p> <p>Publicado em: ' . $timestamp . '</p></p><br>';
            echo '</div>';
        }
    } else {
        echo "0 results found";
    }
}
catch (PDOException $PDOException){
    echo $PDOException;
}
$PDO_connect_posts=null;
$PDO_connect_users=null;
?>