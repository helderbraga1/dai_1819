<?php
/**
 * Created by PhpStorm.
 * User: helder
 * Date: 22/02/19
 * Time: 00:42
 */

include('./db/connection.php'); //Ignorar suposto erro de acesso, temos de ver da perspectiva do ficheiro post.php nao do proprio, works this way.

$sql = "SELECT post.mensagem,tema.nome_tema,post.time,pessoa.firstname,pessoa.lastname 
        FROM post 
        LEFT JOIN pessoa ON pessoa.idPessoa = post.idPessoa 
        LEFT JOIN tema ON post.id_tema = tema.id_tema";
$result = $connection->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $nome_tema = $row['nome_tema'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $mensagem_post = $row['mensagem'];
        $timestamp = $row['time'];
echo '<div style="border: 3px solid gold; padding-left: 2%; margin-top: 1%; margin-left: 2%; margin-right: 2%;margin-bottom: 1%" >';
        echo '<h3>Tema: '.$nome_tema.'</h3><p>'.$mensagem_post.'</p><p style="bold">Utilizador: '. $firstname.' '.$lastname. '</p> <p>Publicado em: '.$timestamp.'</p></p><br>';
    echo '</div>';
    }
} else {
    echo "0 results found";
}
$connection->close();
?>