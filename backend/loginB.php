<?php

include('../db/connection.php');

if(isset($_POST['entrar'])){

    $loginError     = "";
    $formEmail      = $_POST['email'];
    $formPassword 	= $_POST['pass'];
    
    $query = "SELECT * FROM pessoa WHERE email='$formEmail'";
    $result = $conn->query($query);
    
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {

            $idPessoa = $row['idPessoa'];
            $nome1 = $row['firstname'];
			$nome2 = $row['lastname'];
            $email = $row['email'];
			$password = $row['pass'];
            $vip = $row['vip'];
            
        }
        
        if ($formPassword === $password) {
            
            session_start();
            
            $_SESSION['idPessoa'] 			= $idPessoa;
            $_SESSION['firstname'] 		    = $nome1;
			$_SESSION['lastname'] 		    = $nome2;
            $_SESSION['email'] 				= $email;
			$_SESSION['pass'] 			    = $password;
            $_SESSION['vip'] 				= $vip;
            
            header("Location: ../index.php");
            
            
        } else {
            header("Location: ../login.php?alerta=dados-incorretos");
        }
        
    } else {
        header("Location: ../login.php?alerta=utilizador-desconhecido");
    }
    
    $conn->close();

}

?>