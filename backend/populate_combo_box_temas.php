<?php
/**
 * Created by PhpStorm.
 * User: helder
 * Date: 21/02/19
 * Time: 23:44
 * Popula a combobox com as categorias registadas no sistema.
 */


include('./db/connection.php'); //Ignorar suposto erro de acesso, temos de ver da perspectiva do ficheiro post.php nao do proprio, works this way.

$sql = "SELECT id_tema,nome_tema FROM tema";
$result = $connection->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $_id_tema = $row['id_tema'];
        $_nome_tema =$row['nome_tema'];
        echo '<option value="'.$_id_tema.'">'. $_nome_tema. '</option>';
    }
} else {
    echo "0 results found";
}
$connection->close();
?>