<?php
$target = "../img/";
$target = $target . basename( $_FILES['photo']['name']); 

$max_allowed_image_size_MB_helper = 0.25; //MB
$max_allowed_image_size_bytes = (int) round($max_allowed_image_size_MB_helper * 1024*1024); //Bytes

include('../db/connection.php');
include('../includes/session.php');

$idUtilizador = $_SESSION['idPessoa'];

// Submit
if(isset($_POST['inserirPost'])){

    $mensagem = $_POST['mensagem'];
    $tema = $_POST['tema'];
    $pic=($_FILES['photo']['name']); 

    if($mensagem && $tema ) {

        $sql = "INSERT INTO post (id_post, idPessoa, mensagem, tema, photo)
        VALUES (NULL, '$idUtilizador', '$mensagem', '$tema', '$pic')";
        if(move_uploaded_file($_FILES['photo']['tmp_name'],$target))
        {
            if($_FILES['size']<=$max_allowed_image_size_bytes){
                echo "The file " . basename($_FILES['uploadedfile']
                    ['name']) . " has been uploaded, and your information has been added to the directory";
            } else {
                header("Location: ../post.php?alerta=img_exceeds_server_allowed_size");
            }
        } else {
            echo "Sorry, there was a problem uploading your file.";
        }

        if ($conn->query($sql) === TRUE) {
            header("Location: ../post.php?alerta=atividade-inserida");
        }

    } else {

        header("Location: ../post.php?alerta=campos-obrigatorios");
    }
}
?>