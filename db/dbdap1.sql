-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 27, 2019 at 11:56 PM
-- Server version: 10.3.13-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbdap1`
--

-- --------------------------------------------------------

--
-- Table structure for table `pessoa`
--

CREATE TABLE `pessoa` (
  `idPessoa` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `vip` enum('Sim','Não') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pessoa`
--

INSERT INTO `pessoa` (`idPessoa`, `firstname`, `lastname`, `email`, `pass`, `vip`) VALUES
(15, 'Frank', 'Herbert', 'frankherbert@gmail.com', 'Dune', 'Não'),
(19, 'Nico', 'Belic', 'nicobelic@gmail.com', 'russia', 'Não'),
(21, 'Hououin', 'Kyouma', 'madscientist@gate.com', 'Steins', 'Não'),
(22, 'Isaac', 'Asimov', 'isaac@laws.com', 'LawsOfRobotics', 'Não'),
(23, 'Carl', 'Sagan', 'cosmos@nasa.com', 'Cosmos', 'Não'),
(24, 'aaa', 'aaa', 'aaa@gmail.com', 'aaa', 'Sim');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `idPessoa` int(11) NOT NULL,
  `mensagem` varchar(150) CHARACTER SET latin1 NOT NULL,
  `tema` varchar(100) CHARACTER SET latin1 NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `photo` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id_post`, `idPessoa`, `mensagem`, `tema`, `time`, `photo`) VALUES
(20, 24, 'In mel tale aeterno detraxit, ne est iusto repudiare, ex nec detracto intellegebat. Consequuntur signiferumque ad nam, eos ne tibique adversarium cont', 'Saude', '2019-02-26 10:18:24', 'foto1.jpeg'),
(21, 24, 'Primis habemus euripidis duo te, mel saepe dolores an, et mei modo discere debitis. Mel iusto electram in, enim errem efficiantur at mel, sale ceteros', 'Desporto', '2019-02-26 10:23:02', 'ab.jpg'),
(22, 24, 'hhh', 'Entretenimento', '2019-02-26 21:30:58', 'transferir.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tema`
--

CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL,
  `categoria` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tema`
--

INSERT INTO `tema` (`id_tema`, `categoria`) VALUES
(5, 'Comboios'),
(1, 'Desporto'),
(3, 'Entretenimento'),
(2, 'Saúde');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`idPessoa`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- Indexes for table `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_tema`),
  ADD UNIQUE KEY `tema_categoria_uindex` (`categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `idPessoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tema`
--
ALTER TABLE `tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
