#!/bin/bash
filename="start_server.js"

m1=$(md5sum "$filename")
counter=1
echo "Starting server..."
(parallel -j 1 $(node $filename)) &
echo "Success!"
echo "The server has been started $counter times!"
while true; do
  # md5sum is computationally expensive, so check only once every 10 seconds
  sleep 5
  m2=$(md5sum "$filename")

  if [ "$m1" != "$m2" ] ; then
    clear
    echo "WARNING: File has been changed!" >&2 
    m1=$(md5sum "$filename")
    echo "Starting server update..."
    killall node
    echo "Node is dead"
    echo "Restarting the server..."
    (parallel -j 1 $(node $filename)) &
    echo "Server has been updated."
    let counter=counter+1
    echo "The server has been restarted $counter times!"
    echo "Last restarted on:" $(date +"%T %d-%m-%Y")
  fi
done
