<?php
error_reporting(0);
include('includes/session.php');

$alerta = $_GET['alerta'];

if ($alerta == "campos-obrigatorios") {
    $mensagem = "<p>Todos os campos são de preenchimento obrigatório!</p>";
}

if ($alerta == "atividade-inserida") {
	$mensagem = "<p>Post inserido com sucesso!</p>";
}
if($alerta =="img_exceeds_server_allowed_size"){
    $mensagem = "<p>The provided image is over the size upload limit of this server!</p>";
}
session_start();

?>



<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css">
<?php include('./includes/script.php'); ?>
<body>

<nav class="w3-bar w3-black">
  <a href='index.php' class="w3-button w3-bar-item">Home</a>
  
  <?php 
	if($_SESSION['firstname']) {
    echo "<a role='button' class='w3-button w3-bar-item' href='logout.php'>Logout</a>";
    if($_SESSION['vip'] == 'Sim'){
			echo '<a href="registoSelect.php" class="w3-button w3-bar-item">Manage Members</a>';
			echo '<a href="temasSelect.php" class="w3-button w3-bar-item">Manage Topics</a>';
		} else {
			echo '<a href="registoSelect2.php" class="w3-button w3-bar-item">View Members</a>';
			
    }
	} else {
		echo "<a role='button' class='w3-button w3-bar-item'  href='login.php'>Login</a>";
    echo "<a role='button' class='w3-button w3-bar-item'  href='registo.php'>Registar</a>";

    
	}
  ?>
  <a href='post.php' class="w3-button w3-bar-item">Criar Post</a>

</nav>
<br>

<section class="w3-container w3-center" >
  <h2 class="w3-wide">Lorem ipsum dolor sit amet</h2>

	
		<?php echo $mensagem;
		


		?>
		
		<br>
		<h3 class="w3-opacity"><i>Criar post</i></h3>
		<div class="inside">

			<form action="backend/postB.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
						<label for="tema">Tema:<br></label>
						<select class="form-control" id="tema" name="tema">
							<?php
							include('./db/connection.php');
							include('./includes/session.php');
							$temas = mysqli_query($conn, "SELECT * FROM tema ");
							while ($row = mysqli_fetch_array($temas))
							{
								echo "<option value=\"" . $row['categoria'] . "\">" . $row['categoria'] . "</option>";
							}
							?>
						</select>
					</div>
          <br>
					<div class="form-group">
						<label for="msg">Mensagem:<br></label>
						<textarea class="form-control" rows="5" id="msg" name="mensagem"></textarea>
					</div>
					<br>
					<div class="form-group">
						<input type="file" name="photo"><br>
					</div>
					<br>
					<button name="inserirPost" type="submit" class="btn btn-success">Submit</button>
				<br>
			</form>

		</div>
		<h3 class="w3-opacity"><i>Os meus Posts</i></h3>


</body>
</html>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>


<?php
include('./db/connection.php');
include('./includes/session.php');



$idUtilizador = $_SESSION['idPessoa'];

$result = mysqli_query($conn,"SELECT * FROM post WHERE idPessoa = $idUtilizador ");

echo "<table border='1'>
<tr>
<th>Id Post</th>
<th>Id Utilizador</th>
<th>Mensagem</th>
<th>Tema</th>
<th>Data</th>
<th>Foto</th>
</tr>";

while($row = mysqli_fetch_array($result))
{
echo "<tr>";
echo "<td>" . $row['id_post'] . "</td>";
echo "<td>" . $row['idPessoa'] . "</td>";
echo "<td>" . $row['mensagem'] . "</td>";
echo "<td>" . $row['tema'] . "</td>";
echo "<td>" . $row['time'] . "</td>";
echo "<td>" . $row['photo'] . "</td>";
echo "</tr>";
}
echo "</table>";

mysqli_close($conn);
?>
