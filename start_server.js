 
var http = require('http');
var fs = require('fs');
var connection = require('./backend/nodejs/connection.js');
var query_01 = require('./backend/nodejs/query/query_01.js');
http.createServer(function (req, res) {
  fs.readFile('index.php', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });
}).listen(8080); 